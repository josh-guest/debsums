#!/usr/bin/perl -T

# Simple test for additional debsums command-line options

use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my $chroot;
my @command;

# Untaint stuff
delete $ENV{PATH};

# First check if we are testing a source package or an installed package.
my $command = './debsums';
if (exists $ENV{ADTTMP} or exists $ENV{DEBIAN_AS_INSTALLED_TESTING}) {
    $command = '/usr/bin/debsums';
}

ok(-e $command, "Command debsums found at $command");
ok(-x $command, "$command executable");

run($command, '--version');
is(stderr, '', 'STDERR is empty');
# This actually also checks for regressions with regards to #779626.
like(stdout, qr/^debsums \d.\d/, 'Version number is shown');
is(rc >> 8, 0, 'Exit code is 0');

run($command, '--help');
is(stderr, '', 'STDERR is empty');
like(stdout, qr/^Usage: /m, 'Usage message');
is(rc >> 8, 0, 'Exit code is 0');

done_testing();
