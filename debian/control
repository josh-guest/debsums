Source: debsums
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Anders Kaseorg <andersk@mit.edu>,
           Axel Beckert <abe@debian.org>
Section: admin
Priority: optional
### The used debhelper build-dependency (and the debhelper compatibility
### level) of debsums needs to be supported in stable without backports,
### i.e. 10 for stretch (and probably 11 for buster)
Build-Depends: debhelper (>= 10),
               libfile-fnmatch-perl,
               libtest-command-simple-perl,
               po-debconf,
               po4a (>= 0.41)
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/debsums
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/debsums.git
Rules-Requires-Root: no

Package: debsums
Architecture: all
Depends: dpkg (>= 1.16.3),
         libdpkg-perl,
         libfile-fnmatch-perl,
         perl (>= 5.8.0-3),
         ucf (>= 0.28),
         ${misc:Depends},
         ${perl:Depends}
Description: tool for verification of installed package files against MD5 checksums
 debsums can verify the integrity of installed package files against
 MD5 checksums installed by the package, or generated from a .deb
 archive.
